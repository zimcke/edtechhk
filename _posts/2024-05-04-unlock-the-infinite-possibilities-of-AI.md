---
layout: post
title:  "解鎖AI無限可能 齊建社區活力 - Unlock the infinite possibilities of AI and build community vitality"
author: NEXX
categories: [ Workshop ]
tags: [STEM]
image: assets/images/posts/workshops/infinite-possibilities-of-AI.jpeg
description: "生成式人工智能工作坊: - 介紹人工智能(AI)和生成式AI (Gen AI)的概念 - 展示Gen AI工具在內容創作方面的實際應用 - 參與者通過社區主題,實踐練習和發揮創造力 受眾: 公眾,包括10歲及以上的兒童 時長: ~2小時 語言 : 廣東話"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.eventbrite.com/e/ai-tickets-887387027487?aff=ebdssbdestsearch&keep_tld=1
begindate: 2024-05-05 15:00
enddate: 2024-05-05 17:00
comments: false
---

