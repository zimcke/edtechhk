---
layout: post
title:  "Learning and Teaching Expo 2023"
author: lte
categories: [ Educational fair ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/conferences/lte2023.jpg
description: "Asia’s Leading Education Expo and Hong Kong’s annual signature event for educators"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/education-fair.png
category-thumb-lg: assets/images/education-fair.png
registerlink: https://www.ltexpo.com.hk/LTE/
begindate: 2023-12-13 10:00
enddate: 2023-12-15 17:00
comments: false
---

