---
layout: post
title:  "EdTech Showcase"
author: esperanza
categories: [ Webinar ]
tags: [Educators, Teachers, STEM, Business, Lifelong learning]
image: assets/images/posts/webinars/edtech-showcase.jpg
description: "Join EdTech solution providers from around the world to learn how technology can empower and facilitate the learning of languages, STEM and even life skills, and how educators can make blended learning the new normal. Suitable for educators, charity foundations and businesses looking for the new frontiers of doing good and doing well in the education arena."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://app.glueup.com/event/edtech-showcase-54955/
begindate: 2022-06-01 13:30
enddate: 2022-06-01 17:00
comments: false
---

