---
layout: post
title:  "EDUtech ASIA"
author: terrapinn
categories: [ Conference ]
tags: [Educators, Teachers, Researchers, online ]
image: assets/images/conference-room-old.jpg
description: "FAsia’s largest event for education and EdTech providers"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: https://www.terrapinn.com/exhibition/edutech-asia/agenda.stm
begindate: 2021-11-09 09:00
enddate: 2021-11-11 18:00
comments: false
---

