---
layout: post
title:  "STEM teaching practice training course"
author: clc
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Lifelong learning]
image: assets/images/posts/workshops/stem-teacher-training.jpg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://clc.hkfyg.org.hk/2017/12/20/stem%E6%95%99%E5%AD%B8%E5%AF%A6%E8%B8%90%E5%9F%B9%E8%A8%93%E8%AA%B2%E7%A8%8B
begindate: 2023-05-27 14:00
enddate: 2023-06-17 18:00
comments: false
---

日期:	2023年5月27日至6月17日，逢星期六，共4課
時間:	下午2時至6時

課程專為中、小學老師及教學工作者提供有系統及全面的STEM教學培訓，內容涵蓋各類型STEM技術的操作示範與應用，由資深導師教授如何設計STEM課程，並應用不同的技術、教具及教學元素於跨學科的學習活動，讓學員能掌握在校推動STEM的策略與實務技巧。