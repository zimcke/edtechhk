---
layout: post
title:  "Ed Tech Opportunities in Greater Bay Area"
author: esperanza
categories: [ Webinar ]
tags: [Developers, Business, Investors]
image: assets/images/edtech-webinar.jpg
description: ""
featured: false
hidden: false
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.esperanza.life/edtech-opportunities-in-greater-bay-area/
begindate: 2020-08-11 17:00
enddate: 2020-08-11 18:30
comments: false
---

