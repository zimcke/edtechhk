---
layout: post
title:  "Futures of Education and Schooling in the Digital Post-Pandemic Era"
author: lte
categories: [ Webinar ]
tags: [Educators, Teachers, Researchers, post-pandemic, Covid-19 ]
image: assets/images/posts/webinars/lte-future-educatin.png
description: "Having been in the pandemic for more than 20 months, education around the world has experienced unprecedented disruption, yet also unleashed innovations that kept learning going in diverse ways.

Nurturing students with social and emotional skills in this VUCA era is suggested to be one of the key areas in education. In the recent report ‘Back to the Future of Education’ and ‘Beyond Academic Learning - First Results from the Survey of Social and Emotional Skills’ published by the Organisation for Economic Co-operation and Development (OECD), four scenarios for the future of schooling — ‘schooling extended’, ‘education outsourced’, ‘schools as learning hubs’ and ‘learn-as-you-go’ — were investigated. All these imaginative futures encourage us to take a new approach to education by giving priority to learning beyond academic skills. Collaborations with stakeholders from different walks of life will be truly essential to reach all students no matter of their social-economic situations.

The paper ‘Education and COVID-19: Recovering from the Shock Created by the Pandemic and Building Back Better’ published by Professor Fernando M. Reimers also concretely suggested how education can be recovered from the disruption and even built a better and sustainable one to cope with uncertain future disruption.

We are at a pivotal moment in history, and we can incite changes. In this forum, renowned educators, government officials and academics from Estonia, Hong Kong, Singapore, the USA and key international organisations in the education sector will share their valuable experiences and insights, and discuss the following: How should our education systems be evolved and shaped to echo students’ essential needs in the post-pandemic era? How can we recover students’ learning loss during the pandemic and turn it into learning gain in its aftermath? What decisions should we make today in the context of pandemic to bring long-term positive consequences for the future of education?"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://us02web.zoom.us/webinar/register/WN_iOje1cpYTbeIwFpOdehz5w
begindate: 2021-12-1 21:00
enddate: 2021-12-1 22:00
comments: false
---

