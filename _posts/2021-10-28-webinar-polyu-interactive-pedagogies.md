---
layout: post
title:  "Information Session for Project Funding for Strategic Plan: Enhance the Student Learning Experience through the Use of Interactive Pedagogies"
author: polyu
categories: [ Webinar ]
tags: [Educators, Teachers, Developers]
image: assets/images/posts/webinars/interactive-pedagogies.jpg
description: "In this session EDC will explain the Call for Proposals to support the Strategic Plan focus on interactive pedagogies and will include separate discussions on the three focus areas: interactive pedagogies such as flipped classroom and blended learning; incorporate and/or create online and Open Educational resources (OERs); leverage technologies to produce high quality, pedagogically sound learning and teaching"
featured: false
hidden: false
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.polyu.edu.hk/elearning/teacher-support/teacher-training/webinar-information-session-for-project-funding-for-strategic-plan-enhance-the-student-learning-experience-through-the-use-of-interactive-pedagogies/
begindate: 2021-11-05 12:30
enddate: 2021-11-05 14:00
comments: false
---

