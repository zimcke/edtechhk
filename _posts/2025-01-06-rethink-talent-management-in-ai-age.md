---
layout: post
title:  "Rethink talent management in the AI Age"
author: esperanza
categories: [ Workshop ]
tags: [educators, AI, Hong Kong, Future, Talent acquisition, HR ]
image: assets/images/posts/workshops/talent-management.webp
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://app.glueup.com/event/rethink-talent-management-in-the-ai-age-127316/
begindate: 2025-01-16 16:30
enddate: 2025-01-16 20:00
comments: false
---

The rise of AI and digital technologies is fundamentally transforming how organizations attract, develop, and retain talent. This hybrid seminar will explore the critical shifts reshaping talent management and how companies can adapt to thrive in this new landscape. Join experts from Hong Kong, Singapore and the UK to discuss:



1. The Redefinition of Talent: How technology, the Covid-pandemic, shifting job requirements and peoples' changing career aspirations are upending traditional employment modes?

2. Technology as an Enabler: How AI and other technologies are revolutionizing talent acquisition, management and development?

3. HR's Strategic Evolution: How HR functions are transforming from administrative roles to strategic business drivers?



This is a hybrid event. Free attendance for Esperanza community members and online attendance. Non-members can attend in person at HKD300 and enjoy a one year complimentary membership.