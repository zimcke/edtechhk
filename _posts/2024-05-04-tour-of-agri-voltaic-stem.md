---
layout: post
title:  "Tour of Agri-voltaic STEM 農電共生STEM導賞團"
author: slash-social-enterprise
categories: [ Workshop ]
tags: [STEM]
image: assets/images/posts/workshops/tour-of-agri-voltaic-stem.jpeg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.eventbrite.com/e/tour-of-agri-voltaic-stem-stem-tickets-774632385317?aff=ebdssbdestsearch&keep_tld=1
begindate: 2024-05-11 11:00
enddate: 2024-05-11 13:00
comments: false
---

