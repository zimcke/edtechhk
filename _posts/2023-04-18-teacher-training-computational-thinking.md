---
layout: post
title:  "Workshop on Implementing Coding Education to Develop Upper Primary Students’ Computational Thinking"
author: edb
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Lifelong learning]
image: assets/images/posts/workshops/edb-teacher-training.png
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://stem.edb.hkedcity.net/en/events/event/steam-education-learning-teaching-and-assessment-series-workshop-on-implementing-coding-education-to-develop-upper-primary-students-computational-thinking-general-studies-refreshed-3/
begindate: 2023-04-25 14:00
enddate: 2023-04-25 17:00
comments: false
---


To enhance participants’ pedagogical knowledge of using coding education to develop students’ computational thinking in General Studies.
To facilitate participants to promote STEAM education in school through coding education.


提升參加者對於常識科透過編程教育發展學生計算思維的教學知識
促進參加者透過編程教育在學校推行STEAM教育