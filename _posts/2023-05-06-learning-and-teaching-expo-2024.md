---
layout: post
title:  "Learning and Teaching Expo 2024"
author: lte
categories: [ Educational fair ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/conferences/lte2024.jpg
description: "Asia’s Leading Education Expo and Hong Kong’s annual signature event for educators"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/education-fair.png
category-thumb-lg: assets/images/education-fair.png
registerlink: https://www.ltexpo.com.hk/LTE/
begindate: 2024-12-11 10:00
enddate: 2024-12-13 17:00
comments: false
---

With an overarching vision of ‘Shaping Education for a World of Change’, LTE is a professional platform where school leaders and teachers can explore and engage in meaningful discussions on the latest developments in learning and teaching, educational resources and learning technology. 