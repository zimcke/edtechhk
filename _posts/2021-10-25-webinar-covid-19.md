---
layout: post
title:  "EdTech in the wake of COVID-19"
author: fccihk
categories: [ Webinar ]
tags: [Developers, Educators, Teachers, Business, Investors]
image: assets/images/online-head.jpg
description: "The coronavirus has put much of the world in lockdown, leading to the mass shutdown of schools and universities, as well as the adoption of remote working practices. Our approach and tools for both learning and working have consequently undergone significant transformation."
featured: false
hidden: false
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.fccihk.com/events/past-events/2020/webinar-edtech-in-the-wake-of-covid-19.html
begindate: 2020-05-28 00:01
enddate: 2020-05-28 23:59
comments: false
---












