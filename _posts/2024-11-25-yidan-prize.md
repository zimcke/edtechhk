---
layout: post
title:  "Yidan Prize Summit 2024 - Future-proofing Education: the Essential Role of Resilience"
author: yidan
categories: [ Conference ]
tags: [educators, teachers, learning, innovation, resilience, coaching, Hong Kong, future]
image: assets/images/posts/conferences/yidan-price-2024.png
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: https://yidanprize.org/events-and-news/events/2024-yidan-prize-summit?utm_campaign=summit&utm_content=317173780&utm_medium=social&utm_source=facebook&hss_channel=fbp-1619969771657694
begindate: 2024-12-09 09:00
enddate: 2024-12-09 17:00
comments: false
---

Where there is change, there is an opportunity to grow. That applies to individuals, organizations, and systems. And in our drastically unpredictable world, building resilience is crucial to ensure every student can keep learning, learn well, and thrive in the future.

This year, the Summit gathers a wide range of perspectives and experiences to shine a light on what works, and where we still have a way to go. Joining us are educators, researchers, policymakers, and practitioners, who bring their expertise to help us understand and approach resilience in education more holistically.

The discussions will explore:
- fostering resilient learners with the skills to navigate a changing future
- building a support system and looking out for educators
- nurturing resilient education systems across global and local contexts
- how AI impacts resilience in higher education in China and beyond
and feature fireside chats with our 2024 Yidan Prize laureates, Professor Wolfgang Lutz, Professor Mark Jordans, Marwa Zahr, and Luke Stannard.