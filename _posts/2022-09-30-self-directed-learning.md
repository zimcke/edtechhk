---
layout: post
title:  "Self-directed Learning"
author: esperanza
categories: [ Webinar ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/webinars/self-directed-learning.jpg
description: "Schools around the world are embracing the need for technology to be an integral component of quality education. Yet many stakeholders focus on incorporating various tools and apps, presuming transformations in learning will inherently follow. While others view technology in education as a necessary evil at worst, or a mere means to increased productivity at best. 


The International Society for Technology in Education (ISTE)​ has a different vision for technology in education, which is why it developed the ISTE Standards​. At their core, the ISTE Standards are about pedagogy, not tools. Which is to say, they emphasize the ways that technology can be used to amplify and even transform learning and teaching. 



We are seeing trends in the field of education with more people realise the insufficiency of throwing digital tools into classrooms. Without further support, it is hard to expect valid changes in teaching and, more importantly, improved student outcomes. What has not been fully realized, however, is the potential for technology to mend gaps in equity, engage students as unique individuals and prepare them for an uncertain future. The ISTE Standards​ have been designed to accelerate this potential and drive innovation in education. "
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://app.glueup.com/event/iste-x-esperanza-asia-education-summit-60871/
begindate: 2022-10-8 9:00
enddate: 2022-10-8 12:15
comments: false
---

