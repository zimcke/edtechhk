---
layout: post
title:  "The Crossroads of Education & Future of Work in the Post-Pandemic Era"
author: unesco
categories: [ Conference ]
tags: [Educators, Teachers, Researchers, post-pandemic, Covid-19 ]
image: assets/images/posts/webinars/future_education.jpeg
description: "Futures of Education is UNESCO's global initiative to reimagine how knowledge and learning can shape the future of humanity and the planet. The theme this year is The crossroads of education & future of work in the post-pandemic era."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: https://asiasociety.org/hong-kong/events/crossroads-education-future-work-post-pandemic-era?fbclid=IwAR2OeYYsR2OMJrNWpTDtMMMIj4Ml2E-4NwfrJSp4DDH-8ukiyieSa2iL_w4
begindate: 2021-11-10 14:00
enddate: 2021-11-10 18:00
comments: false
---

