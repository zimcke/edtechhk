---
layout: post
title:  "Teacher Sharing Session: A Better Way to “Meet” Your Students Online (Gather Town)"
author: polyu
categories: [ Webinar ]
tags: [Educators, Teachers]
image: assets/images/posts/webinars/meet-students-gather.jpg
description: ""
featured: false
hidden: false
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.polyu.edu.hk/elearning/teacher-support/teacher-training/webinar-teacher-sharing-session-a-better-way-to-meet-your-students-online/
begindate: 2021-11-02 12:00
enddate: 2021-11-02 13:00
comments: false
---

