---
layout: post
title:  "Workshop - Code for Fun"
author: esperanza
categories: [ Workshop ]
tags: [Students, Educators, Teachers ]
image: assets/images/posts/workshops/code-for-fun.jpg
description: "Students, teachers and parents are invited to join Coding for Fun to learn what coding is and why it is important for the future of our children. Participants can join in person at Cyberport or online. Admission is free. Shuttle bus will be provided at Admiralty.

Participants of the CoderZ League 2021 will share their learning experiences. Some of them will compete with a CEO team in exciting missions. Students can have a taste of CoderZ in a one hour workshop – both online or in person. Parents and teachers can also learn coding together with their children/students through the CoderZ Amazon Cyber Robotics Challenge"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://app.glueup.com/event/code-for-fun-47211/
begindate: 2022-01-22 9:30
enddate: 2022-01-22 13:00
comments: false
---

