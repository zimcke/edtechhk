---
layout: post
title:  "Learning and Teaching Expo 2022"
author: lte
categories: [ Educational fair ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/conferences/lte2022.jpg
description: "Asia’s Leading Education Expo and Hong Kong’s annual signature event for educators"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/education-fair.png
category-thumb-lg: assets/images/education-fair.png
registerlink: https://www.ltexpo.com.hk/LTE/
begindate: 2022-12-07 10:00
enddate: 2022-12-9 17:00
comments: false
---

