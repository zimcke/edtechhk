---
layout: post
title:  "EdTech in Hong Kong: Join the Education Revolution in Asia"
author: investHK
categories: [ Webinar ]
tags: [Educators, Investors, Teachers, Researchers ]
image: assets/images/posts/webinars/investhk-edtech.png
description: "The event focused on the last trends of Hong Kong EdTech ecosystem, Hong Kong's role as a launch pad to Mainland China and Asia Pacific and sharing experiences by EdTech companies in Europe using Hong Kong as their hub in Asia. The participants would got a better understanding of how effective their EdTech solutions could be in Asian markets, some of the business and funding models in the region and examples of cross-sector and cross-border opportunities tapping into Hong Kong market."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.investhk.gov.hk/en/events/webinar-edtech-hong-kong-join-education-revolution-asia.html
begindate: 2021-04-21 10:00
enddate: 2021-04-21 12:00
comments: false
---

