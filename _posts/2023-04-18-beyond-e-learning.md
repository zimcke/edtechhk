---
layout: post
title:  "Beyond e-Learning: How might we co-learn with AI and other technologies?"
author: esperanza
categories: [ Conference ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/conferences/beyond-e-learning.png
description: "Just as we think learning can resume normalcy, ChatGPT is making waves in the education world. Pundits believe that 2023 will be a defining year of AI and the future of learning – for schools and the workplace alike. "
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: https://app.glueup.com/event/beyond-e-learning-73079/
begindate: 2023-04-14 15:45
enddate: 2023-04-14 19:00
comments: false
---


Join this hybrid seminar if you would like to find out the global trends in #EdTech applications in #K12 education and #adultlearning, and the needs of educators and L&D professionals in Hong Kong. Prof Rose Luckin, one of the world's most influential educators, will be the keynote speaker. We shall also announce a new EdTech Heroes Award to celebrate early adopters of EdTech.
<br>Free online or offline participation via: <a href="https://bit.ly/3xFPpAK">bit.ly/3xFPpAK</a>

<h2>超越電子學習: 如何與人工智能和其他科技共同學習？</h2>

正當我們認為學習可以復常，#ChatGPT 正在教育界掀起波瀾。 權威人士認為，不論是學校或職場，2023 年將是#人工智能和未來學習模式決定性的一年。 請即登記這混合硏討會，了解教育科技在#K12與#成人學習上應用的全球趨勢以及香港學校和企業的需求。世界上最具影響力的教育家之一Rose Luckin 教授將擔任主講嘉賓。會上並會介紹全新推出的EdTech Heroes Award 以表彰採用學習科技的的先行者。
<br>請登入連結，免費報名缐上或缐下參與: <a href="https://bit.ly/3xFPpAK">bit.ly/3xFPpAK</a>
