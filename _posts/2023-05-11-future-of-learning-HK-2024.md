---
layout: post
title:  "Future of Learning HK 2024"
author: appledistedu
categories: [ Conference ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/conferences/future-of-learning.jpeg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference.png
registerlink: https://folade.my.canva.site/hk2024
begindate: 2024-11-08 00:00
enddate: 2024-11-09 23:59
comments: false
---

The Future of Learning Hong Kong Conference aims to unite a community of forward-thinking educators to share ideas, collaborate, and discover innovative methods for authentic learning.