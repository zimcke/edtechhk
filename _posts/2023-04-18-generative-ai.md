---
layout: post
title:  "Generative AI: The AI Revolution is here - what now?"
author: economicclub
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Business, Lifelong learning]
image: assets/images/posts/workshops/generative_ai.jpg
description: "Are we at the start of a wonderful productivity explosion and are the voiced concerns unfounded?"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://www.eventbrite.com/e/generative-ai-the-ai-revolution-is-here-what-now-tickets-615052146527?aff=ebdssbdestsearch
begindate: 2023-04-18 19:00
enddate: 2023-04-18 21:30
comments: false
---



Generative AI, especially Chat-GPT have made AI a mainstream topic. Even to the point that Elon Musk and other tech experts call for ‘pause’ on advanced AI systems. Are we at the start of a wonderful productivity explosion and are the voiced concerns unfounded? Welcome to join an upcoming meeting of The Economic Club of Hong Kong "Generative AI: The AI Revolution is here - what now?", which will be held on Tue April 18th from 20:00 till 21:30 (doors open at 19:30) at Social Room (3/F, 74-78 Stanley Street, Central).
