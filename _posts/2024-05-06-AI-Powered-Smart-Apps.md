---
layout: post
title:  "AI-Powered Smart Apps: Build Smart Apps That Learn and Adapt"
author: wtia
categories: [ Workshop ]
tags: [STEM, AI, programming]
image: assets/images/posts/workshops/ai-apps.jpeg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.eventbrite.com/e/ai-powered-smart-apps-build-smart-apps-that-learn-and-adapt-tickets-894153295577?aff=ebdssbdestsearch&keep_tld=1
begindate: 2024-06-07 15:00
enddate: 2024-06-07 17:00
comments: false
---

