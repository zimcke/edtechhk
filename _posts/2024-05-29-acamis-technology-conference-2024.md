---
layout: post
title:  "ACAMIS Technology Conference - Innovation for Impact"
author: acamis
categories: [ Conference ]
tags: [AI, robotics, educators, programming, developers, teachers, learning, innovation, maker education, coaching, Hong Kong]
image: assets/images/posts/conferences/acamis-technology-conference.jpg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: https://www.acamis.org/professional-development/technology-conference
begindate: 2024-10-19 00:00
enddate: 2024-10-20 23:59
comments: false
---

Join the ACAMIS Tech Conference 2024, where "Innovation for Impact" comes alive! This October 19-20, dive into a world of possibilities as we explore the transformative power of technology in education. Whether you're passionate about coaching, learning innovations, maker education, or robotics, this conference is your chance to connect with like-minded educators, share your talents, and discover cutting-edge practices that will revolutionize your students' learning experience. Don't miss this opportunity to be part of the EdTech community dedicated to making a lasting impact through innovative education!
