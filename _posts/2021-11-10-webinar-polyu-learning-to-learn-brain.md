---
layout: post
title:  "Learning to Learn Series: Balancing Teacher-directed & Student-directed Teaching & Learning by Knowing How Our Brain Learns"
author: polyu
categories: [ Webinar ]
tags: [Educators, Teachers, Developers]
image: assets/images/posts/webinars/learning-to-learn.png
description: "Knowing more about how our brain learns helps us to better design and deliver courses to our students. In this session, we will refer to some interesting concepts presented in the recently published book “Uncommon Sense Teaching”, authored by Prof Barbara Oakley and her team, and shed light on how students learn, effective pedagogies and good practices already adopted by our fellow teachers.
"
featured: false
hidden: false
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.polyu.edu.hk/elearning/teacher-support/teacher-training/cop-webinar-learning-to-learn-series-balancing-teacher-directed-student-directed-teaching-learning-by-knowing-how-our-brain-learns-re-run/
begindate: 2021-11-17 12:30
enddate: 2021-11-17 13:45
comments: false
---

