---
layout: post
title:  "Compelling Curricular Design For Distance Learning"
author: bsd
categories: [ Webinar ]
tags: [Developers, Educators, Teachers]
image: assets/images/online-learning.jpg
description: "Hybrid learning has forced educators to rethink their curriculum design to account for equity, effectiveness and engagement. Lessons are often expected to work synchronously, asynchronously, and a blend of the two. Learn from expert BSD curriculum designers how they’re building high quality curricula that uses teacher time efficiently."
featured: false
hidden: false
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://us02web.zoom.us/meeting/register/tZ0sd-GhqzoiG9xjrAPAmbt_ZbtN8NrRHVSu
begindate: 2021-10-27 12:00
enddate: 2021-10-27 13:00
comments: false
---