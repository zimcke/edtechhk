---
layout: post
title:  "𝐑𝐨𝐚𝐝𝐦𝐚𝐩 𝐭𝐨 𝐋𝐞𝐚𝐝𝐞𝐫𝐬𝐡𝐢𝐩: 𝐀𝐜𝐡𝐢𝐞𝐯𝐞 𝐒𝐮𝐜𝐜𝐞𝐬𝐬 𝐟𝐨𝐫 𝐅𝐮𝐭𝐮𝐫𝐞 𝐂𝐚𝐫𝐞𝐞𝐫"
author: fosterhk
categories: [ Conference ]
tags: [Students, Educators]
image: assets/images/posts/conferences/roadmap-to-leadership-forum.jpg
description: "Career Education Forum co-organized by 𝐅𝐨𝐬𝐭𝐞𝐫 𝐖𝐨𝐫𝐥𝐝𝐰𝐢𝐝𝐞 x 𝐄𝐧𝐚𝐜𝐭𝐮𝐬 𝐇𝐨𝐧𝐠 𝐊𝐨𝐧𝐠. Top-tier professionals from different industries will share career advice and discuss how the current education system can equip students with the key industry skills. The organization wants to empower students to accelerate their career and get them ready for any heated industry.
Boosting Hong Kong’s talent competitiveness and building a sustainable future for our next generation"
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: http://www.fosterhongkong.org/roadmap-to-leadership.html
begindate: 2021-11-28 15:30
enddate: 2021-11-28 18:00
comments: false
---

