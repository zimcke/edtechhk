---
layout: post
title:  "2021 Edventures GBA Summit: Think Big with EdTech"
author: edventures
categories: [ Acceleration programme ]
tags: [Acceleration, Investors, Startups, Developers]
image: assets/images/hk-md.jpg
description: "The Summit is the highlight of the Edventures Global Business Acceleration (GBA) Fellowship and a featured event of the Cyberport Venture Capital Forum. The 2021 Edventures GBA Fellows will present their solutions from early childhood education to lifelong learning."
featured: true
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/startup.png
category-thumb-lg: assets/images/startup-lg.png
registerlink: https://www.edventuresgba.com/en/summit.html
begindate: 2021-11-03 9:30
enddate: 2021-11-03 21:30
comments: false
---

