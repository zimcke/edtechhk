---
layout: post
title:  "Get Started with AI"
author: esperanza
categories: [ Workshop, Webinar ]
tags: [AI, educators, programming, developers, teachers, online]
image: assets/images/posts/webinars/esperanza-get-started-with-ai.jpg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://app.glueup.com/event/get-started-with-ai-103029/
begindate: 2024-06-07 16:00
enddate: 2024-06-07 19:00
comments: false
---

AI is set to transform the world of learning. It has the potential to make learning more engaging, personalised, adaptive, inclusive, efficient and effective. But there are concerns about misinformation, bias, privacy, academic integrity and dehumanising the learning experience.

Are you ready to reap the opportunities and manage the risks of using AI? Designed for educators and learning & development professionals, this forum gathers international experts and local practitioners to look into:

- Developing an AI readiness roadmap for your organisation
- The first steps to using AI in teaching and staff development
- Using AI to innovate learning : Case of Singapore
- How we might support the uptake of AI in teaching and learning in Hong Kong

![Get Started with AI poster](/assets/images/posts/workshops/get-started-with-ai.jpg)
