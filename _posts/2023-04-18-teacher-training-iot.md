---
layout: post
title:  "Professional Development Programme on Innovation and Technology for STEAM Co-ordinators of Schools – IoT and Art Tech"
author: edb
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Lifelong learning]
image: assets/images/posts/workshops/edb-teacher-training.png
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://stem.edb.hkedcity.net/zh-hant/%E6%B4%BB%E5%8B%95/%E6%B4%BB%E5%8B%95/steam%e6%95%99%e8%82%b2%e7%9f%a5%e8%ad%98%e5%a2%9e%e7%9b%8a%e7%b3%bb%e5%88%97%ef%bc%9a%e5%ad%b8%e6%a0%a1steam%e7%b5%b1%e7%b1%8c%e4%ba%ba%e5%93%a1%e5%89%b5%e6%96%b0%e7%a7%91%e6%8a%80%e5%b0%88-4/
begindate: 2023-05-06 09:30
enddate: 2023-05-06 17:00
comments: false
---


To update participants with the latest development on Innovation and Technology (I&T);
To broaden participants’ understanding of IoT and Art Technology;
To illustrate to participants on how to incorporate I&T elements in STEAM education; and
To provide opportunities for sharing good practical experience among STEAM co-ordinators on coordinating and planning STEAM education


讓參加者獲得有關創新科技最新發展的資訊；
拓寬參加者對物聯網及藝術科技的認識；
讓參加者了解如何在STEAM教育中引入創科元素；以及
提供機會讓STEAM統籌人員分享統籌及規劃STEAM教育的良好實踐經驗。