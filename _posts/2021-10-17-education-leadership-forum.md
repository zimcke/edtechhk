---
layout: post
title:  "Education Leadership Forum"
author: oxford
categories: [ Conference ]
tags: [Educators, Teachers, Researchers, Covid-19, ]
image: assets/images/grow.jpg
description: "Learning beyond tomorrow."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/conference.png
category-thumb-lg: assets/images/conference-lg.png
registerlink: https://juven.io/oupchina/elf/registration?utm_source=eventbrite&utm_medium=referral&utm_campaign=education_forum
begindate: 2021-11-05 09:00
enddate: 2021-11-06 18:00
comments: false
---

