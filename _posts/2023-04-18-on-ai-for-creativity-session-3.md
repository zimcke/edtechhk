---
layout: post
title:  "On AI For Creativity - Session 3"
author: meetcai
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Business, Lifelong learning]
image: assets/images/posts/workshops/on-ai-for-creativity.jpg
description: "“On AI For Creativity” is a series of sharing sessions where we invite experts from various domains to share ideas on AI x Creativity."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://www.eventbrite.com/e/on-ai-for-creativity-session-3-tickets-613314268487?aff=ebdssbdestsearch&keep_tld=1
begindate: 2023-04-25 19:00
enddate: 2023-04-25 21:00
comments: false
---



This session will be held in Central, Hong Kong (3 mins from MTR station) where we will focus on discussing AI-generated image space. Join for a relaxing night of learning, sharing and bonding with our technologists, designers, entrepreneurs, directors, and other curious minds.

🚀Talk 1: “Try It On” With AI (by Nathan)

“Try it On" is a web app Nathan co-founded which allows anyone to create stylish professional headshots with Generative Tooling. Nathan will also share tips on starting up with Generative AI. See a review by Allie here.

Presenter Bio: Nathan is MIT-trained and had held various quantitative and engineering roles at MIT, AI teams such as Apple's Siri, quant funds, and many others. He had also involved with instructing Data Science and Machine learning in Hong Kong.

🚀Talk 2: Pose-dependent Generation with ControlNet (by Julius)

For tech-enabled designers and architects, it’s a known problem that text-to-image model has limits on subject and pose-dependent generation, making it harder to collaborate with humans. Julius will share an app project that utilizes a technique from Stanford which easily adds additional conditional inputs such as "edge maps" to text-to-image model Stable Diffusion (an open-source text-to-image model); this allows people to create visuals with the structure they desire in addition to text prompts.

Presenter Bio: Julius is a data scientist from Hong Kong’s Health Care industry with well-rounded stacks and experiences including building state-of-the-art disease prediction from imagery data.

Agenda:

6:50 - 7:10 pm (Check-in & Opening Monologue)<br>
7:10 - 8:30 pm (Presentations & Discussion)<br>
8:30 - 9:00 pm (Networking + Complimentary 🍷 )<br>


📝 Notes:

(1) Seating will be at around 65% of the venue capacity and on first-come-first-serve basis. Please do arrive on time to prevent interruption during the talks.

(2) Please release the ticket should you no longer be able to attend, this allows the wait-listed to join and support our effects to provide better experience for our guests. :)

See ya creatives! 😊