---
layout: post
title:  "Construction Green Workshop - STEM Handicrafts"
author: cic
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Business, Lifelong learning]
image: assets/images/posts/workshops/stem-handicrafts.jpg
description: "“On AI For Creativity” is a series of sharing sessions where we invite experts from various domains to share ideas on AI x Creativity."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://www.eventbrite.com/e/-stem-stem-handicrafts-tickets-595462413107?aff=ebdssbdestsearch&keep_tld=1
begindate: 2023-04-29 15:00
enddate: 2023-04-29 17:00
comments: false
---


建造綠色工作坊 - STEM科學物理小手作
