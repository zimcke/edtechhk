---
layout: post
title:  "「STEAM教育規劃 – Robot in STEAM」學與教 : 六足及格鬥機械人教師工作坊(初階及進階程度)"
author: edb
categories: [ Workshop ]
tags: [Educators, Teachers, STEM, Lifelong learning]
image: assets/images/posts/workshops/edb-teacher-training.png
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://stem.edb.hkedcity.net/en/events/event/%e3%80%8csteam%e6%95%99%e8%82%b2%e8%a6%8f%e5%8a%83-robot-in-steam%e3%80%8d%e5%ad%b8%e8%88%87%e6%95%99-%e5%85%ad%e8%b6%b3%e5%8f%8a%e6%a0%bc%e9%ac%a5%e6%a9%9f%e6%a2%b0%e4%ba%ba%e6%95%99/
begindate: 2023-04-28 14:00
enddate: 2023-05-05 17:00
comments: false
---
