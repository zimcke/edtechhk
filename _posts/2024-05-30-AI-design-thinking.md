---
layout: post
title:  "Experience design thinking with AI // 體驗AI X 設計思維"
author: esperanza
categories: [ Workshop ]
tags: [AI, educators, programming, developers, teachers, design thinking]
image: assets/images/posts/workshops/ai-design-thinking.jpg
description: ""
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/workshop.png
category-thumb-lg: assets/images/workshop-lg.png
registerlink: https://www.eventbrite.com/e/ai-x-design-thinking-workshop-unleashing-creativity-with-promptathon-tickets-909612454337?aff=oddtdtcreator&fbclid=IwZXh0bgNhZW0CMTAAAR2onmsq4tME92smsr73fqEvB9HK05_hgxffW7Zqvi9gScZaXnVHYI544to_aem_AY3Do42hbsI2RuvgSDn4oRnyWSPBbnc4-bnPTf2Yf1gcRCO2vpJzvLYE5rTwgVxVdlvEOP9w8lmsBRTuFWkH_ftK
begindate: 2024-06-28 16:30
enddate: 2024-06-28 18:30
comments: false
---
Join this #artificialintelligence x #designthinking workshop on 28 June. Prof. Andy Chun, a veteran AI champion and expert in strategic and technology innovation, will take participants on a journey of #innovation by harnessing the power of Generative AI and design thinking. The traditional hackathon will be transformed into a promptathon, where prompts drive breakthroughs in solving your organisation challenges. Prior knowledge of generative AI/design thinking is NOT required.

The workshop is exclusive for Esperanza community members but we have reserved a limited number of seats for non-members. 


參加 6 月 28 日舉辦的 #人工智能聯乘 #設計思維工作坊。資深人工智能倡導者、策略和科創新專家 Andy Chun 教授將使 #生成人工智能和設計思維，帶領參與者踏上 #創新之旅。傳統的黑客馬拉松將轉變為promptathon，透過prompts 推動突破，解決您組織的挑戰。參與者不需要具有人工智能/設計思維的知識。

工作坊專為薯片叔叔共創社社群成員開放，但我們為非會員設立了若干席位。

