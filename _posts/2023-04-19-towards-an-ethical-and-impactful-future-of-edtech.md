---
layout: post
title:  "Towards an ethical and impactful EdTech: A dialogue between East and West"
author: rinalai-hillman
categories: [ Webinar ]
tags: [Educators, Teachers, STEM]
image: assets/images/posts/webinars/future-of-edtech.jpg
description: "A dialogue between the East and West: networking event to discuss impactful investment needed to drive ethical EdTech innovation."
featured: false
hidden: false
mapping:
    latitude: 51.101
    longitude: 0.1
category-thumb: assets/images/webinar.png
category-thumb-lg: assets/images/webinar-lg.png
registerlink: https://www.eventbrite.com/e/towards-an-ethical-and-impactful-edtech-a-dialogue-between-east-and-west-tickets-557707497137
begindate: 2023-04-25 17:00
enddate: 2023-04-25 18:00
comments: false
---

