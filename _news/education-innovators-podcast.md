---
layout: resource
title:  "Exploring AI and Education with the Education Innovators Podcast"
categories: [ Podcast ]
tags: [ educators, innovation, AI, podcast, ethics, tools, insights, community, leadership]
thumbnail: assets/images/news/education-innovators-podcast-logo.png
short: Since its launch in September 2023, the Education Innovators Podcast has rapidly become an essential resource for those passionate about educational technology, reaching over 50,000 downloads. Hosted by Eric Byron, this podcast offers a platform for educational leaders to share their stories of success and innovation. By inviting guests from various backgrounds in education, Eric explores practical and inspirational tales of transformative approaches in teaching and learning.
comments: false
---

## A Growing Resource for EdTech Enthusiasts

Since its launch in September 2023, the **[Education Innovators Podcast](https://educationinnovatorspodcast.podbean.com/)** has rapidly become an essential resource for those passionate about educational technology, reaching over 50,000 downloads. Hosted by **[Eric Byron](https://www.facebook.com/eric.byron.79/)**, this podcast offers a platform for educational leaders to share their stories of success and innovation. By inviting guests from various backgrounds in education, Eric explores practical and inspirational tales of transformative approaches in teaching and learning.

<img align="right" width="200" height="200" style="border-radius: 10px;" src="/assets/images/news/eric-byron.jpg">

Eric Byron, an experienced podcaster and educator based in Hong Kong, masterfully blends his rich background in technology and education to spotlight pivotal issues and innovations. Each episode is crafted to steer the conversation from what's wrong in the educational landscape to what's working and what holds promise for the future. Through discussions with experts and thought leaders, the podcast opens up meaningful dialogues on the potential of new educational practices and tools.

## Insightful Discussions on Ethical AI

The latest episode provides a compelling example of the cutting-edge topics explored on the podcast. Here, Eric tackles the ethical and moral implications of developing AI applications, such as intelligent tutors, by experimenting with children. This solo episode uniquely features personal insights, framing a significant discussion about the role and responsibility of AI in education. 

### Engage with EdTech Conversations

Explore the podcast on **[YouTube](https://www.youtube.com/@ericbyron8262)** or **[Podbean](https://educationinnovatorspodcast.podbean.com/)** to engage with discussions in EdTech.