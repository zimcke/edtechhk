---
layout: page
title: Credits
permalink: /credits
comments: false
---

## Image Credits

* [Acceleration-programme.png](https://www.edtechhongkong.org/assets/images/categories/Acceleration-programme.png) -- Created by <a href="https://www.flaticon.com/free-icon/startup_2672304" title="startup">nawicon</a>
* [Educational-fair.png](https://www.edtechhongkong.org/assets/images/categories/Educational-fair.png) -- Created by <a href="https://www.flaticon.com/free-icon/presentation_2997513" title="presentation">nawicon</a>
* [Webinar.png](https://www.edtechhongkong.org/assets/images/categories/Webinar.png) -- Created by <a href="https://www.flaticon.com/free-icon/webinar_2997554" title="webinar icons">nawicon</a>
* [Conference.png](https://www.edtechhongkong.org/assets/images/categories/Conference.png) -- Created by <a href="https://www.freepik.com/icon/conference_2511948" title="conference">nawicon</a>
* [Workshop.png](https://www.edtechhongkong.org/assets/images/categories/Workshop.png) -- Created by <a href="https://www.flaticon.com/free-icons/puzzle" title="puzzle icons">nawicon</a>
* [X.png](https://www.edtechhongkong.org/assets/images/footer/x.png) -- Created by <a href="https://www.freepik.com/icon/twitter_11823292" title="twitter">Freepik</a>
* [Community.jpg](https://www.edtechhongkong.org/assets/images/about/community.jpg) -- Photo by <a href="https://unsplash.com/@wildlittlethingsphoto?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Helena Lopes</a> on <a href="https://unsplash.com/photos/photo-of-three-person-sitting-and-talking-UZe35tk5UoA?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
* [Quote-newsletter.jpg](https://www.edtechhongkong.org/assets/images/quote-newsletter.jpg) -- Photo by <a href="https://unsplash.com/@frostroomhead?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Rodion Kutsaiev</a> on <a href="https://unsplash.com/photos/woman-wearing-black-top-standing-near-yellow-wall-IJ25m7fXqtk?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  
