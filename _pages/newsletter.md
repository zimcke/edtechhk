---
layout: default
title: Subscribe to our newsletter
permalink: /newsletter
comments: false
---
<div class="container">
    <div class="row">
        <div class="section-title col-12">
            <h2 class="mk-fancy-title pattern-style"><span>{{ page.title }}</span></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6 col-xs-12 pl-0">
            <p>Discover the future of education with EdTech Hong Kong's periodic newsletter! Stay ahead of the curve with exclusive insights into cutting-edge EdTech events, innovative articles, invaluable resources, and promising startups transforming the landscape of learning.</p>
            <div id="mc_embed_shell">
                    <div id="mc_embed_signup">
                        <form action="https://edtechhongkong.us5.list-manage.com/subscribe/post?u=d410506bdcf6454e5e7cea91f&amp;id=bc85761eb0&amp;f_id=008d32eaf0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_self" novalidate="">
                            <div id="mc_embed_signup_scroll">
                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                <div class="mc-field-group"><label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label><input type="email" name="EMAIL" class="required email" id="mce-EMAIL" required="" value=""></div><div class="mc-field-group"><label for="mce-FNAME">First Name </label><input type="text" name="FNAME" class=" text" id="mce-FNAME" value=""></div><div class="mc-field-group"><label for="mce-LNAME">Last Name </label><input type="text" name="LNAME" class=" text" id="mce-LNAME" value=""></div><div class="mc-field-group input-group"><strong>Tell us something about your background </strong><ul><li><input type="radio" name="MMERGE6" id="mce-MMERGE60" value="Teaching professional"><label for="mce-MMERGE60">Teaching professional</label></li><li><input type="radio" name="MMERGE6" id="mce-MMERGE61" value="Business owner"><label for="mce-MMERGE61">Business owner</label></li><li><input type="radio" name="MMERGE6" id="mce-MMERGE62" value="Developer"><label for="mce-MMERGE62">Developer</label></li><li><input type="radio" name="MMERGE6" id="mce-MMERGE63" value="Investor"><label for="mce-MMERGE63">Investor</label></li><li><input type="radio" name="MMERGE6" id="mce-MMERGE64" value="Other"><label for="mce-MMERGE64">Other</label></li></ul></div>
                                <div id="mce-responses" class="clear foot">
                                    <div class="response" id="mce-error-response" style="display: none;"></div>
                                    <div class="response" id="mce-success-response" style="display: none;"></div>
                                </div>
                                <div aria-hidden="true" style="position: absolute; left: -5000px;">
                                /* real people should not fill this in and expect good things - do not remove this or risk form bot signups */
                                    <input type="text" name="b_d410506bdcf6454e5e7cea91f_bc85761eb0" tabindex="-1" value="">
                                </div>
                                <div class="optionalParent">
                                    <div class="clear foot">
                                        <input type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-warning" value="Subscribe to our newsletter">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-xs-12 pl-0 mb-3">
            <img src="/assets/images/quote-newsletter.jpg" alt="Don't miss out on the opportunity to be part of a thriving community driving educational innovation. Subscribe now and lead the charge in shaping tomorrow's classrooms today!">
        </div>
    </div>
</div>